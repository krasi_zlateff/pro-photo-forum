var express = require('express');
var app = express();

// set the port of our application
// process.env.PORT lets the port be set by Heroku
var port = process.env.PORT || 8080;

// set the view engine to ejs
app.set('view engine', 'ejs');

// make express look in the public directory for assets (css/js/img)
app.use(express.static(__dirname + '/public'));
// app.use(express.static(__dirname + '/javascripts'));
// app.use(express.static(__dirname + '/bower_libraries'));

// set the home page route
app.get('/', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('index');
});

// set the about us page route
app.get('/about-us', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('about_us');
});

// set the contacts page route
app.get('/contacts', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('contacts');
});

// set the terms of use page route
app.get('/terms-of-use', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('terms_of_use');
});

// set the photographers page route
app.get('/photographers', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('photographers');
});

// set the photos page route
app.get('/photos', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('photos');
});

// set the photographer's gallery page route
app.get('/photographer-gallery', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('photographer_gallery');
});

// set the photographer's messages page route
app.get('/photographer-msg-all', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('photographer_msg_all');
});

// set the photographer's single message page route
app.get('/photographer-msg-single', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('photographer_msg_single');
});

// set the registration page route
app.get('/registration', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('registration');
});

// set the join pro page route
app.get('/join-pro', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('join_pro');
});

// set the admin page route
app.get('/admin', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('admin');
});

// set the admin edit page route
app.get('/admin-edit', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('admin_edit');
});

// set the profile page route
app.get('/profile', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('profile');
});

// set the profile edit page route
app.get('/profile-edit', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('profile_edit');
});

// set the interview page route
app.get('/interview-form', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('interview_form');
});

// set the interview page route
app.get('/interview', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('interview');
});

// set the photo-stories page route
app.get('/photo-stories', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('photo_stories/photo_stories');
});

// set the photo-stories preview page route
app.get('/photo-stories-preview', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('photo_stories/photo_stories_preview');
});

// set the photo-stories upload page route
app.get('/photo-stories-upload', function (req, res) {
  // ejs render automatically looks in the views folder
  res.render('photo_stories/photo_stories_upload');
});

app.listen(port, function () {
  console.log('Our app is running on http://localhost:' + port);
});
