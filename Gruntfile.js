module.exports = function(grunt){
    "use strict";

    require('time-grunt')(grunt);
    require('jit-grunt')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        concat: {
            options: {
                separator: ';'
            },
            dist: {
                src: [
                    'bower_libraries/bower_components/jquery/dist/jquery.min.js',
                    'bower_libraries/bower_components/toastr/toastr.min.js',
                    'javascripts/bootstrap-slider.min.js',
                    'javascripts/jquery-flexslider.js',
                    'javascripts/bootstrap.min.js',
                    'javascripts/bootstrap-sprockets.js',
                    'javascripts/bootstrap-select.js',
                    'javascripts/bootstrap-toggle.js',
                    'javascripts/main.js'
                ],
                dest: 'public/dist/scripts.js'
            }
        },
        uglify: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n'
            },
            dist: {
                files: {
                    'public/dist/scripts.min.js': ['<%= concat.dist.dest %>']
                }
            }
        },
        jshint: {
            files: ['Gruntfile.js', 'public/js/**/*.js'],
            options: {
                globals: {
                    jQuery: true,
                    console: true,
                    module: true
                }
            }
        },
        compass: {
            compassCompile: {
                options: {
                    config: 'config.rb',
                    sassDir: 'sass',
                    cssDir: 'public/dist',
                    imagesDir: 'public/images',
                    force: true
                }
            },
            server: {
                options: {
                    config: 'config.rb',
                    sassDir: 'sass',
                    cssDir: 'public/dist',
                    imagesDir: 'public/images',
                    force: true,
                    watch: true
                }
            }
        },
        clean: {
            dist: {
                src: ['public/dist/*']
            }
        },
        watch: {
            css: {
                files: 'sass/**/*.scss',
                tasks: ['compassCompile']
            },
            livereload: {
                options: {
                    livereload: 35729
                },
                files: ['public/css/**/*.css', 'views/*.ejs', 'javascripts/**/*.js']
            }
        }
    });

    //TODO: creating appropiate HTML file
    grunt.registerTask('dist', [
        'clean',
        'compass:compassCompile',
        // 'jshint',
        'concat',
        'uglify'
    ]);

    grunt.registerTask('compassCompile', [
        'compass:compassCompile'
    ]);

    grunt.registerTask('server', [
        'compass:compassCompile',
        'compass:server'
    ]);

    grunt.registerTask('live', [
        'compass:compassCompile',
        'watch'
    ]);
};
