(function ($) {
    $(document).ready(function () {

        // Top Menu - Show drop down menu
        var topMenu,
            hasSubmenu,
            otherHasSubmenus,
            subMenu,
            mobileMenuWrapper,
            mobileMenuIcon,
            mobileMenuElement,
            mobileMenuCloseButton,
            mobileMenuBackButton;

        // Menu Main
        topMenu = $('.top-header .top-menu');
        hasSubmenu = $(topMenu).find('.menu-item.has-submenu');
        subMenu = $(hasSubmenu).find('.submenu');

        // Mobile Menu
        mobileMenuWrapper = $('.top-header .mobile-menu-wrapper');
        mobileMenuIcon = $('.top-header .mobile-menu .mobile-menu-hamburger');
        mobileMenuBackButton = $(mobileMenuWrapper).find('.mobile-menu-nav .menu-item:first-child');
        mobileMenuCloseButton = $(mobileMenuWrapper).find('.mobile-menu-nav .menu-item .mobile-menu-close');
        mobileMenuElement = $(mobileMenuWrapper).find('.top-menu .menu-item .menu-element');


        // Submenu
        $(hasSubmenu).off('click').on('click', function (e) {
            otherHasSubmenus = $(hasSubmenu).not($(this));

            if ($(this).hasClass('active')) {
                $(this).removeClass('active');
                $(subMenu).removeClass('active');
                $(mobileMenuBackButton).addClass('hidden');
            } else {
                $(this).addClass('active');
                $(subMenu).addClass('active');
                $(otherHasSubmenus).removeClass('active');
                $(mobileMenuBackButton).removeClass('hidden');
            }
        });

        // Mobile Menu
        $(mobileMenuIcon).off('click').on('click', function (e) {
            e.preventDefault();

            $(mobileMenuWrapper).stop().show();
        });

        $(mobileMenuCloseButton).off('click').on('click', function (e) {
            e.preventDefault();

            $(mobileMenuWrapper).stop().hide();
        });

        // Mobile menu back button
        $(mobileMenuBackButton).off('click').on('click', function (e) {
            e.preventDefault();

            $(subMenu).removeClass('active');
            $(hasSubmenu).removeClass('active');
            $(this).addClass('hidden');
        });

        // Mobile menu element
        $(mobileMenuElement).off('click').on('click', function (e) {
            $(mobileMenuWrapper).stop().hide();
        });

        // Custom checkbox
        $('.custom-checkbox').off('click').on('click', function (e) {
            e.preventDefault();

            if ($(this).hasClass('checked')) {
                $(this).removeClass('checked');
            } else {
                $(this).addClass('checked');
            }
        });

        // Custom toggle checkbox
        $('.custom-switch-checkbox').bootstrapToggle({
            on: '<i class="fa fa-check"></i>',
            off: '<i class="fa fa-times"></i>',
            width: 70,
            height: 32
        });

        // Home Page
        // --------- //
        // Smooth scroll to all photographers
        $('.home-page .main-header .arrow-down').off('click').on('click', function (e) {
            e.preventDefault();

            $('html, body').animate({
                scrollTop: $('.photographers-all').offset().top
            }, 800);
        });

        // https://silviomoreto.github.io/bootstrap-select/options/
        $('select').selectpicker({
            size: 4,
            tickIcon: 'fa fa-check-square-o',
            showTick: true,
            showIcon: true
        });

        // Photographer Gallery - FlexSlider
        $('.category-photo-slider').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            sync: ".category-photo-carousel"
        });

        // Photographer Gallery - FlexSlider
        $('.category-photo-carousel').flexslider({
            animation: "slide",
            controlNav: false,
            animationLoop: false,
            slideshow: false,
            itemWidth: 90,
            itemMargin: 2,
            asNavFor: '.category-photo-slider'
        });

        // Photographer Gallery - Show photo info
        $('.main-photo-viewer .photo-controls .btn-photo-info').off('click').on('click', function (e) {
            e.preventDefault();

            var photoControls, photoInfo;

            photoControls = $(this).closest('.photo-controls');
            photoInfo = $(photoControls).find('.photo-info');

            if ($(photoInfo).hasClass('active')) {
                $(photoInfo).removeClass('active');
            } else {
                $(photoInfo).addClass('active');
            }
        });


        // Join Pro Page - Smooth scroll to account type information
        $('.join-pro-page .main-header .arrow-down').off('click').on('click', function (e) {
            e.preventDefault();

            $('html, body').animate({
                scrollTop: $('.container-wrapper').offset().top
            }, 800);
        });

        // Join Pro Page - Show/Hide Features
        var proFeature = $('.pro-feature-wrapper');
        var proFeatureHeader = $(proFeature).find('.pro-feature-header');
        var proFeatureBody = $(proFeature).find('.pro-feature-body');

        $(proFeatureHeader).off('click').on('click', function (e) {
            e.preventDefault();

            if ($(this).next($(proFeatureBody)).hasClass('active')) {
                $(this).next($(proFeatureBody)).stop().slideUp();
                $(this).next($(proFeatureBody)).removeClass('active');
            } else {
                $(this).next($(proFeatureBody)).stop().slideDown();
                $(this).next($(proFeatureBody)).addClass('active');
            }
        });

        // Join Pro Page - Period plan slider
        var perionPlanWrapper,
            periodSeparator,
            otherPeriodSeparators,
            currentPeriodSeparator,
            periodFilled,
            nextPeriodSeparators,
            prevPeriodSeparators;

        perionPlanWrapper = $('.period-plan');
        periodSeparator = $(perionPlanWrapper).find('.period-separator');
        periodFilled = $(perionPlanWrapper).find('.period.period-filled');

        periodSeparator.off('click').on('click', function (e) {
            e.preventDefault();

            currentPeriodSeparator = $(this);
            otherPeriodSeparators = periodSeparator.not(currentPeriodSeparator);
            nextPeriodSeparators = currentPeriodSeparator.nextAll('.period-separator');
            prevPeriodSeparators = currentPeriodSeparator.prevAll('.period-separator');

            if (!currentPeriodSeparator.hasClass('current')) {
                otherPeriodSeparators.removeClass('current');
                nextPeriodSeparators.removeClass('period-filled');
                prevPeriodSeparators.addClass('period-filled');
                currentPeriodSeparator.addClass('current period-filled');
                periodFilled.attr('id', currentPeriodSeparator.attr('data-target'));
            }
        });

        // Choose payment method
        var paymentMethod,
            otherPaymentMethod;

        paymentMethod = $('.payment-methods-list .payment-method');

        $(paymentMethod).off('click').on('click', function (e) {
            e.preventDefault();

            otherPaymentMethod = $(paymentMethod).not($(this));

            if (!$(this).hasClass('selected')) {
                $(this).addClass('selected');
                otherPaymentMethod.removeClass('selected');
            } else {
                $(this).removeClass('selected');
            }
        });

        // Photographers Page - Price Range
        $("#photographers-price-ranger").slider({
            tooltip: 'always',
            tooltip_split: true
        });

        // Show toast messages.
        // https://github.com/CodeSeven/toastr
        // http://codeseven.github.io/toastr/demo.html

        // Show success toastr
        $('.btn-success').off('click').on('click', function () {
            toastr.success('Запази');
        });

        // Show info toastr
        $('.btn-info').off('click').on('click', function () {
            toastr.info('Инфо');
        });

        // Show warning toastr
        $('.btn-warning').off('click').on('click', function () {
            toastr.warning('Внимание');
        });

        $('.btn-danger').off('click').on('click', function () {
            toastr.error('Изтрий');
        });

        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": true,
            "positionClass": "toast-top-right",
            "preventDuplicates": true,
            "onclick": null,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "5000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };

        // Change Photo Stories Grid View
        var gridPhotos = $('.row-grid-photos');
        $('#change-list-view').off('click').on('click', function (e) {
            e.preventDefault();
            $(gridPhotos).removeClass('grid-view');
            $(gridPhotos).addClass('list-view');
        });

        $('#change-grid-view').off('click').on('click', function (e) {
            e.preventDefault();
            $(gridPhotos).removeClass('list-view');
            $(gridPhotos).addClass('grid-view');
        });

        // Photo stories main photo show info
        $('.photo-stories .single-storie .main-photo').off('click').on('click', function () {
            if (!$(this).hasClass('storie-info-active')) {
                $(this).addClass('storie-info-active');
                $(this).find('.stories-info').stop().animate({
                    opacity: 1
                }, 200);
            } else {
                $(this).removeClass('storie-info-active');
                $(this).find('.stories-info').stop().animate({
                    opacity: 0
                }, 200);
            }
        });

        // Top header notifications on click
        var userAccountTool = $('.user-account-tools-wrapper .user-account-tool');
        var userNotificationsListWrapper = $(userAccountTool).find('.user-notifications-list-wrapper');

        // if the target of the click isn't the userNotificationsListWrapper nor a descendant of the userNotificationsListWrapper
        $(document).mouseup(function (e) {
            if (!userNotificationsListWrapper.is(e.target) && userNotificationsListWrapper.has(e.target).length === 0) {
                userNotificationsListWrapper.hide();
            }
        });

        $(userAccountTool).off('click').on('click', function (e) {
            e.preventDefault();
            var thisUserNotificationsListWrapper, otherUserNotificationsListWrapper;

            thisUserNotificationsListWrapper = $(this).find($(userNotificationsListWrapper));
            otherUserNotificationsListWrapper = $(userNotificationsListWrapper).not($(thisUserNotificationsListWrapper));

            if ($(thisUserNotificationsListWrapper).hasClass('active')) {
                $(thisUserNotificationsListWrapper).removeClass('active');
            } else {
                $(thisUserNotificationsListWrapper).addClass('active').show();
                $(otherUserNotificationsListWrapper).removeClass('active').hide();
            }
        });

    });

    $(window).load(function () {
        var slideList,
            slideListPhoto,
            slideListPhotoImage,
            slideListPhotoImageSrc,
            slideListPhotoWidth,
            slideListPhotoHeight;

        slideList = $('.main-photo-viewer .category-photo-slider .slides');
        slideListPhotoImage = $(slideList).find('li.slide-item .slide-photo img');

        var loadPhotoImage = function () {
            $(slideListPhotoImage).each(function () {
                // Hack!!! attach current time behind the image path in order to prevent caching
                $(this).attr('src', $(this).attr('src') + '?' + new Date().getTime())
            }).on('load', function () {
                slideListPhotoWidth = $(this).width();
                slideListPhotoHeight = $(this).height();
                slideListPhotoImageSrc = $(this).attr('src');

                if (slideListPhotoWidth > slideListPhotoHeight) {
                    slideListPhoto = $(this).closest('.slide-photo');
                    slideListPhoto.css('background-image', 'url("' + slideListPhotoImageSrc + '")');
                    $(this).hide();
                }
            });
        };
        // Hack!!! set timeout in order to let the rendering threads catch up.
        setTimeout(loadPhotoImage, 0);

        // ===================
        // ----------

        // Scrolling tabs
        var panelHeading = $('.panel .panel-heading');
        var navPrevTabs = $(panelHeading).find('.nav-prev-tabs');
        var navNextTabs = $(panelHeading).find('.nav-next-tabs');
        var navTab = $(panelHeading).find('.nav.nav-tabs li');

        // Get all nav-tabs width and sum them in order to make sliding area
        var itemsTotalWidth = function (selector) {
            var navTabWidth = 0;
            $(selector).each(function () {
                navTabWidth += parseInt($(this).outerWidth(true));
            });
            return navTabWidth;
        };

        var navTabsAllWidth = itemsTotalWidth(navTab);
        var scrollingNavTabsWidth = itemsTotalWidth(navTab) * 2;

        // Sliding area
        var navTabsSlidingArea = $('.panel .panel-heading .nav-tabs-wrapper');
        var navTabsSlidingAreaWidth = parseInt($(navTabsSlidingArea).outerWidth(true));

        // show arrows if tabs are not able to fit in scrolling area
        if (navTabsAllWidth > navTabsSlidingAreaWidth) {
            $(navPrevTabs).show();
            $(navNextTabs).show();
            $(panelHeading).addClass('scrolling-tabs');
            $(panelHeading).find('.nav.nav-tabs').addClass('nav-tabs-scrolling');
            var panelHeadingScrolling = $('.panel .panel-heading.scrolling-tabs');
            var scrollingNavTabs = $(panelHeadingScrolling).find('.nav.nav-tabs.nav-tabs-scrolling');
            $(navTab).first().addClass('current-scrolling-tab');
            $(scrollingNavTabs).css('width', scrollingNavTabsWidth);
        } else {
            $(scrollingNavTabs).css('width', 'auto');
            $(scrollingNavTabs).css('display', 'inline-block');
        }

        $(navTab).off('click').on('click', function () {
            if (!$(this).hasClass('current-scrolling-tab')) {
                $(navTab).removeClass('current-scrolling-tab').not($(this));
                $(this).addClass('current-scrolling-tab');
            }
        });

        // Button next tabs
        $(navNextTabs).off('click').on('click', function (e) {
            e.preventDefault();

            var currentScrollingTab = $(panelHeading).find('.nav.nav-tabs li.current-scrolling-tab');
            var currentScrollingTabWidth = parseInt($(currentScrollingTab).outerWidth(true));

            var nextNavTab = $(currentScrollingTab).nextAll($(navTab)).first();
            var lastNavTab = $(currentScrollingTab).nextAll($(navTab)).last();
            var prevNavTab = $(currentScrollingTab).prevAll($(navTab));

            var nextAllNavTabsWidth = itemsTotalWidth($(currentScrollingTab).nextAll($(navTab)));
            var lastNavTabWidth = parseInt($(lastNavTab).outerWidth(true));

            $(currentScrollingTab).removeClass('current-scrolling-tab');

            if ($(nextNavTab).length) {
                $(nextNavTab).addClass('current-scrolling-tab');

                var currentLeft = currentScrollingTabWidth;

                var leftToScroll = nextAllNavTabsWidth - navTabsSlidingAreaWidth;

                if (currentLeft > 0) {
                    currentLeft = currentLeft + itemsTotalWidth(prevNavTab);
                    if (leftToScroll < 0) {
                        currentLeft = currentLeft + (leftToScroll * -1);
                    }
                    if (navTabsSlidingAreaWidth > (nextAllNavTabsWidth + lastNavTabWidth)) {
                        $(navTab).removeClass('current-scrolling-tab').first().addClass('current-scrolling-tab');
                        currentLeft = 0;
                    }
                }
                $(scrollingNavTabs).stop().animate({
                    left: -currentLeft
                }, 400);
            } else {
                currentLeft = 0;
                $(navTab).first().addClass('current-scrolling-tab');
                $(scrollingNavTabs).stop().animate({
                    left: currentLeft
                }, 400);
            }
        });

        // Button previous tabs
        $(navPrevTabs).off('click').on('click', function (e) {
            e.preventDefault();

            var currentScrollingTab = $(panelHeading).find('.nav.nav-tabs li.current-scrolling-tab');
            var currentLeft = parseInt($(scrollingNavTabs).css('left'));
            var prevNavTab = $(currentScrollingTab).prevAll($(navTab)).first();
            var prevAllNavTabs = $(currentScrollingTab).prevAll($(navTab));
            var prevAllNavTabsWidth = parseInt($(prevAllNavTabs).outerWidth(true));

            if (currentLeft < 0) {

                $(currentScrollingTab).removeClass('current-scrolling-tab');
                $(prevNavTab).addClass('current-scrolling-tab');

                currentLeft = currentLeft + parseInt($(prevNavTab).outerWidth(true));

                if ((currentLeft + prevAllNavTabsWidth) > 0) {
                    currentLeft = 0;
                    $(navTab).removeClass('current-scrolling-tab');
                    $(navTab).first().addClass('current-scrolling-tab');
                }

                $(scrollingNavTabs).stop().animate({
                    left: currentLeft
                });
            } else {
                return;
            }
        });

    });
})(jQuery);
